import java.util.*;
public class Assignment16 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		boolean repeat = true;
		do {
			System.out.println("Enter the first integer: ");
			int n1 = scan.nextInt();
			System.out.println("Enter the second integer");
			int n2 = scan.nextInt();
			int sum = n1 + n2;
			System.out.println("The sum of two given numbers are: " + sum);
			System.out.println("Enter true to perform operation again and false to exit");
			repeat = scan.nextBoolean();
		} while(repeat);


	}

}
