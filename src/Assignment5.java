import java.util.*;
public class Assignment5 {

	public static void main(String[] args) {
		int temp = 0;
		int [] buildingHeights = {20,25,30,34,38,42,48,55,65,72};
//Arranging heights in descending order:
		for(int a=0; a<buildingHeights.length; a++) {
			for(int b=a+1; b<buildingHeights.length; b++) {
				if (buildingHeights[a]<=buildingHeights[b]) {
					temp = buildingHeights[a];
					buildingHeights[a] = buildingHeights[b];
					buildingHeights[b] = temp;
				}
			}			
		}
		
		System.out.println("New Array in descending order is: " + Arrays.toString(buildingHeights));

		System.out.println("The top 3 buildings are: ");
		for(int h=0; h<3; h++) {
			System.out.println(buildingHeights[h]);
		}


	}

}

